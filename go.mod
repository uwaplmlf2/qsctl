module bitbucket.org/uwaplmlf2/qsctl

go 1.17

require (
	bitbucket.org/uwaplmlf2/qs v0.4.2-0.20220412202327-23b5878f5f12
	github.com/gomodule/redigo v1.8.8
	github.com/urfave/cli v1.22.5
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
)
