// Qsctl provides a CLI to control a Quicksilver Iridium modem.
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/uwaplmlf2/qs"
	"github.com/gomodule/redigo/redis"
	"github.com/urfave/cli"
)

var Version = "dev"
var BuildDate = "unknown"

func publishMsg(conn redis.Conn, topic string, t time.Time, data interface{}) error {
	rec := struct {
		T    time.Time   `json:"time"`
		Data interface{} `json:"data"`
	}{T: t.Truncate(time.Millisecond), Data: data}
	buf, err := json.Marshal(rec)
	if err != nil {
		return err
	}

	_, err = conn.Do("PUBLISH", topic, buf)
	return err
}

func main() {
	app := cli.NewApp()
	app.Name = "qsctl"
	app.Usage = "CLI interface to Quicksilver Iridium modem"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "url",
			Usage:  "Quicksilver Web API URL",
			Value:  "https://192.168.20.1/api",
			EnvVar: "QS_URL",
		},
		cli.StringFlag{
			Name:   "u",
			Usage:  "Quicksilver username:password",
			Value:  "",
			EnvVar: "QS_USER",
		},
		cli.VersionFlag,
	}

	var cln *qs.Client
	app.Before = func(c *cli.Context) error {
		var err error
		cln, err = qs.NewClient(c.String("url"))
		if err != nil {
			return err
		}

		var ui *url.Userinfo
		if creds := c.String("u"); creds != "" {
			f := strings.Split(creds, ":")
			if len(f) < 2 {
				return fmt.Errorf("Invalid credentials: %q", creds)
			}
			ui = url.UserPassword(f[0], f[1])
		}

		return cln.Session(ui)
	}

	app.After = func(c *cli.Context) error {
		cln.CloseSession()
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:  "info",
			Usage: "output modem info in JSON format",
			Action: func(c *cli.Context) error {
				stat, err := cln.IridiumStatus()
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Operation failed: %v", err), 2)
				}

				info, err := cln.IridiumInfo()
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Operation failed: %v", err), 2)
				}

				sim, err := cln.IridiumSim()
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Operation failed: %v", err), 2)
				}

				result := struct {
					Stat qs.IrdConstellation `json:"stat"`
					Info qs.IrdHw            `json:"info"`
					Sim  qs.IrdSim           `json:"sim"`
				}{Stat: stat, Info: info, Sim: sim}
				json.NewEncoder(os.Stdout).Encode(result)
				return nil
			},
		},
		{
			Name:      "monitor",
			Usage:     "publish Iridium signal quality to Redis",
			ArgsUsage: "INTERVAL",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:   "host",
					Usage:  "Redis server host:port",
					Value:  "localhost:6379",
					EnvVar: "REDIS_HOST",
				},
				cli.StringFlag{
					Name:   "chan",
					Usage:  "Redis channel for published data",
					Value:  "data.ird.status",
					EnvVar: "REDIS_HOST",
				},
				cli.DurationFlag{
					Name:   "refresh",
					Usage:  "QS login refresh interval",
					Value:  time.Minute * 10,
					EnvVar: "QS_SESSION_REFRESH",
				},
			},
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				interval, err := time.ParseDuration(c.Args().First())
				if err != nil {
					cli.NewExitError(fmt.Errorf("%s: %v", c.Args().First(), err), 2)
				}
				conn, err := redis.Dial("tcp", c.String("host"))
				if err != nil {
					cli.NewExitError(fmt.Errorf("%s: %v", c.String("host"), err), 2)
				}

				sigs := make(chan os.Signal, 1)
				signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
				defer signal.Stop(sigs)

				log.Println("Monitoring Iridium signal ... ")
				tick1 := time.NewTicker(interval)
				tick2 := time.NewTicker(c.Duration("refresh"))
				for {
					select {
					case t0 := <-tick1.C:
						ic, err := cln.IridiumStatus()
						if err != nil {
							log.Printf("Cannot check Iridium status: %v", err)
							return nil
						}
						publishMsg(conn, c.String("chan"), t0, ic)
					case <-tick2.C:
						if err := cln.Refresh(); err != nil {
							log.Printf("Cannot refresh session: %v", err)
							return nil
						}
					case sig := <-sigs:
						log.Printf("Caught signal: %v, exiting", sig)
						return nil
					}
				}
			},
		},
		{
			Name:      "wifi",
			Usage:     "enable or disable Wifi interface",
			ArgsUsage: "on|off",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				var enable bool
				switch arg := strings.ToLower(c.Args().First()); arg {
				case "on", "enable", "yes", "1":
					enable = true
				case "off", "disable", "no", "0":
					enable = false
				default:
					return cli.NewExitError(fmt.Errorf("Invalid argument: %s", arg), 1)
				}
				if err := cln.WifiEnable(enable); err != nil {
					return cli.NewExitError(fmt.Errorf("Operation failed: %w", err), 2)
				}

				return nil
			},
		},
		{
			Name:      "mode",
			Usage:     "set Iridium interface mode",
			ArgsUsage: "data|inactive",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				var enable bool
				switch arg := strings.ToLower(c.Args().First()); arg {
				case "data":
					enable = true
				case "inactive":
					enable = false
				default:
					return cli.NewExitError(fmt.Errorf("Invalid argument: %s", arg), 1)
				}
				if err := cln.IridiumActivate(enable); err != nil {
					return cli.NewExitError(fmt.Errorf("Operation failed: %w", err), 2)
				}

				return nil
			},
		},
	}
	app.Run(os.Args)
}
